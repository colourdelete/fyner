package app

import (
	"fmt"
	"fyne.io/fyne"
	"fyne.io/fyne/app"
	"fyne.io/fyne/container"
	"fyne.io/fyne/dialog"
	"fyne.io/fyne/theme"
	"fyne.io/fyne/widget"
	"log"
)

type App struct {
	fyne.App
}

func New(id string) *App {
	return &App{App: app.NewWithID(id)}
}

func (a *App) NewWindow(title string) *Window {
	win := &Window{
		Window: a.App.NewWindow(title),
		app:    a,
		Actions: &Actions{actions: map[string]Action{
			"settings": NativeAction{
				callback: func(window *Window, child string) error {
					dialog.NewCustomConfirm(
						"Settings",
						"OK",
						"Cancel",
						container.NewMax(widget.NewLabel("Settings test")),
						func(confirmed bool) {
							if confirmed {
								log.Print("settings dialog ok")
							} else {
								log.Print("settings dialog cancel")
							}
						},
						window,
					).Show()
					return nil
				},
				icon:  theme.SettingsIcon(),
				label: "Settings",
			},
			"test": NativeAction{
				callback: func(win *Window, child string) error {
					fmt.Println("test")
					return nil
				},
				icon:  theme.DocumentIcon(),
				label: "Test",
			},
		}},
	}
	win.SyncActions()
	return win
}
