module fyner

go 1.15

require (
	fyne.io/fyne v1.4.3
	github.com/traefik/yaegi v0.9.11 // indirect
)
